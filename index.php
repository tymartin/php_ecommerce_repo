<?php

	header('location: ./views/home.php'); 
?>

<!-- RECAP:

-> Translate and interpret an ERD for the PHP ECOMMERCE.
-> create a database sql file.
-> create a file structure and layout for the following contents
		-> login
		-> register
		-> catalog
		-> template
		-> home
		->cart
-> Queries (Advance Selects)
	-> JOIN
	-> COUNT
	-> DISTINCT
	-> LIKE
	-> ORDER BY
	-> IN
	-> ALL

SCOPES AND OBJECTIVES FOR TODAY:

At the end of the session, Students should be able to:

1. Complete the following: 
	-> connect Mysql + PHP.(not graded).
	-> CREATE a registration form with validation. (graded) 
	-> create a login page with sessions. (graded)
	-> create an Add Category Page with Full CRUD. (graded) 
	-> CREATE an ADD PRODUCT PAGE with FULL CRUD. (graded).

During Capstone making
	-> Less instructions from the teacher/instructor. 
	
	rule: 

	1 question per day for each bootcamper.

	Capston 2: Asset Management(Laravel Blade)


REMINDER: 

-> Kindly start completing for any defficiencies. 
-> online portfolio 

 -->