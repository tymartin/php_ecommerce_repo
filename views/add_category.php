<?php

	require_once './../partials/template.php'; 

	function get_content(){?>
	<div class="container">
		<div class="row my-5">
			<div class="col-12 col-sm-10 col-md-8 mx-auto">
				
				<form action="./../controllers/process_category.php" method="post">
					<div class="form-group">
						<label for="name">Category Name:</label>
						<input type="text" name="category-name" id="catName" class="form-control">

					</div>

					<button type="submit" class="btn btn-primary">Add Category</button>
				</form>

				<hr> 
				
				<ul>
				<?php

					require './../controllers/connection.php'; 

					$sql_query = "SELECT name, id from categories";

					$result = mysqli_query($conn, $sql_query); 
					while($name = mysqli_fetch_assoc($result)){?> 
						<li>
							<?php echo "{$name['name']}" ?> 
							<br> 
							<a href="./../controllers/edit_category_form.php?id=<?php echo $name['id'] ?>" class="btn btn-success">Edit Category</a>
							<a href="./../controllers/delete_category.php?id=<?php echo $name['id']?>" class="btn btn-danger">Delete Category</a>
						</li>
					<?php }; 
					?>
				<ul> 

			</div>
		</div>
	</div>

<?php };  ?> 