<?php
	//invoke the session
	session_start(); 

	//catch the product id and place it inside a container
	$id = $_GET['id'];

	//destroy the product inside the session cart
	unset($_SESSION['cart'][$id]); 

	//create a control structure that will check whenever the items saved on the CART SESSION will be 0..then the cart will destroyed
	if(count($_SESSION['cart']) === 0){
		unset($_SESSION['cart']); 
	}


	//browser redirect to the previous page
	header("location: {$_SERVER['HTTP_REFERER']}"); 
?> 