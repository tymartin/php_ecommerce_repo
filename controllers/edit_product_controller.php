<?php

// catch the product id
$product_id = $_GET['id'];

//get/catch the data from the edit_product.php
// product name
$product_name = htmlspecialchars(trim($_POST['product-name']));
// product price
$product_price = htmlspecialchars(trim($_POST['product-price']));
// product category
$product_category = htmlspecialchars(trim($_POST['product-category']));
// product description
$product_description = htmlspecialchars(trim($_POST['product-description']));

//Now we have to validate the entered datas before saving in our database. 

//check if file is selected for the image
function checkIfFileSelected($file){
	if (empty($_FILES['product-image']['name'])) {
		return false;
	} else {
		return true;
	}

}
//now create a function that will check the file type & size of the image 
function saveFile($file){
	$filename = $file['name'];
	$filesize = $file['size'];
	$filetype = strtolower(pathinfo($file['name'],PATHINFO_EXTENSION));
	$file_tmp_name = $file['tmp_name'];

	//validation names

	//validation that will check for the file type
	$is_not_image = true;
	//validation to check if the file is more than 0 but less than the limit
	$is_empty_file = true;

	if (
		$filetype == "jpg" ||
		$filetype == "jpeg" ||
		$filetype == "png" ||
		$filetype == "gif" ||
		$filetype == "svg" 
	)
	{
	$is_not_image = false;
	}
	if ($filesize > 0) {
		$is_empty_file = false;
	}
	if (!$is_empty_file && !$is_not_image) {
		$destination = "./../assets/images/$filename";
		move_uploaded_file($file_tmp_name, $destination);

		return $destination; 
	}
	return false;		
}
//create a function to check if all inputs have data
function checkInputIsComplete($name,$price,$category,$description){
	if (
		empty($name) ||
		empty($price) ||
		empty($category) ||
		empty($description) 
	)
	{
		return false;
	} else {
		return true;
	}
}

if (checkInputIsComplete($product_name, $product_price, $product_category, $product_description)){
	require './../controllers/connection.php';

	if (checkIfFileSelected($_FILES['product-image'])) {
		$image = saveFile($_FILES['product-image']);

	
	//query to update files in the database
	$sql_update_product = "UPDATE products 
	SET 
		name='{$product_name}',
		price={$product_price},
		category_id={$product_category},
		description='{$product_description}',
		image='$image'
	WHERE
		id = {$product_id}
	";

	//connect the database and query
	mysqli_query($conn, $sql_update_product);
	//browser redirect if Update is successful
	header("Location: ./../views/catalog.php?id={$product_id}");
	

} else {
		$sql_update_product = "UPDATE products 
	SET 
		name='{$product_name}',
		price={$product_price},
		category_id={$product_category},
		description='{$product_description}'
	WHERE
		id = {$product_id}
	";

	// echo $sql_update_product;
	mysqli_query($conn, $sql_update_product);
	header("Location: ./../views/catalog.php?id={$product_id}");
	}

	
} else {
	//browser redirect if update is not successful
	header("Location: {$_SERVER['HTTP_REFERER']}");
}


?>

<!-- trim() -> strips white spaces(or other characters) from the beginning and end of a string. -->

<!--  htmlspecialchar() -> converts spacial characters to HTML entities -->