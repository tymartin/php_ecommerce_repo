<?php

	//invoke session
	session_start(); 

	//create a control structure that will make sure that the user is logged in when making a transaction 
	if(!isset($_SESSION['user'])){
	  //if the session is empty the browser redirect to login so the user can login first.
	  header("location : ./../views/login.php"); 
	} 

	$total = 0;
	$product_id = join(",",array_keys($_SESSION['cart']));

	//invoke the database
	require_once "./connection.php"; 

	//create a query to get all the products from the database 
	$sql_query_get_cart = "SELECT * FROM products WHERE id IN ($product_id)"; 

	//connect database with the query
	$result = mysqli_query($conn, $sql_query_get_cart);
	while ($product = mysqli_fetch_assoc($result)) {
		$subtotal = $product['price'] * $_SESSION['cart'][$product['id']]; 
		$total += $subtotal; 
	}

	//get the details of the customer and details of the transactions
	$transaction_code = generateTransactionCode();
	$user_id = $_SESSION['user']['id']; 
	//catch the type of payment mode from cart.php 
	$payment_mode_id = $_POST['payment-mode'];
	$status_id = 1;   

	function generateTransactionCode() {
		$transaction_code = "";
		$chars = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']; 
		for ($i=0; $i <5; $i ++){
			$index = rand(0,15);

			$transaction_code = $transaction_code . $chars[$index]; 
		}

		$transaction_code = $transaction_code . getdate()[0]; 

		return $transaction_code;
	} 

	//create a query to add the transaction details inside the table 
	$sql_query_add_transaction = "INSERT INTO 
	transactions (
			transaction_code, 
			total, 
			user_id, 
			status_id,
			payment_mode_id 

	)
	VALUES (
	'$transaction_code',
	$total, 
	$user_id, 
	$status_id, 
	$payment_mode_id
	)"; 
	
	$result = mysqli_query($conn, $sql_query_add_transaction); 

	$transaction_id = mysqli_insert_id($conn);

	$array_entries = []; 
	foreach ($_SESSION['cart'] as $id => $quantity) {
		$array_entries[] = "($transaction_id, $id, $quantity)"; 
	}

	$values = join(",",$array_entries);

	$query = "INSERT INTO product_transactions(
		transaction_id,
		product_id,
		quantity
		) VALUES 
		$values"; 

	mysqli_query($conn, $query); 

	//at the event of the clear cart
	unset($_SESSION['cart']); 

	//browser redirect going to transactions.php
	header("location: ./../views/transactions.php"); 
?>