<?php

	//invoke the database
	require_once "connection.php";

	session_start(); 
	
	//catch the data
	$product_name = $_POST['product-name'];
	$product_price = $_POST['product-price'];
	$product_category = $_POST['product-category'];
	$product_description = $_POST['product-description'];

	//List of validations that im going to use with their default values

	$incomplete_fields = true; 
	$is_empty_file = true; 
	$is_not_image = true; 

	//Validation to check if fields have data( not empty)
	if(
		empty($product_name) ||
		empty($product_price) ||
		empty($product_category) ||
		empty($product_description) ||
		!is_numeric($product_price)
	){
		$incomplete_fields = true; 
	} else {
		$incomplete_fields = false; 
	} 

	$product_image = $_FILES['product-image']; 
	$product_image_name = $product_image['name'];
	$product_image_size = $product_image['size'];
	$product_image_type = strtolower(pathinfo($product_image_name, PATHINFO_EXTENSION));
	$product_image_tmpname = $product_image['tmp_name']; 

	//validation to check if file is empty and also to set a LIMIT
	if($product_image_size > 0 && $product_image_size < 800000){
		echo "Not an empty File"; 
		$is_empty_file = false; 
	} 

	//validation to check the file format (JPEG, JPG, PNG, GIF, SVG)
	if(
		$product_image_type == "jpg" ||
		$product_image_type == "png" ||
		$product_image_type == "jpeg"||
		$product_image_type == "gif" ||
		$product_image_type == "svg"
	){
		echo "Is an image File!"; 
		$is_not_image = false; 
	}   


	//to insert data inside the database if all requirement are valid

	if(!$incomplete_fields && !$is_empty_file && !$is_not_image){

		$product_image_destination = "./../assets/images/". date("Y-m-d-h-i-s")."$product_image_name"; 
		move_uploaded_file($product_image_tmpname, $product_image_destination); 

		//insert query to insert data in the database
		$cat_insert = "INSERT INTO `products` (`name`, `price`, `description`, `image`, `category_id`) VALUES ('$product_name', '$product_price','$product_description', '$product_image_destination', '$product_category')"; 

		mysqli_query($conn, $cat_insert); 
		echo "Your file is Successfully uploaded!"; 
		//Browser redirect
		header('location: ./../views/add_product.php');
	} else {
		echo "Error in uploading data!"; 
		//Browser redirect
		$_SESSION['error message'] = "All fields are required!"; 
		header('location: ./../views/add_product.php'); 
		
	} 

 

?>