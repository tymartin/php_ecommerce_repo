<?php

	//host
	$host = 'localhost';
	//db_username
	$db_username = 'root';
	//db_password
	$db_password = '';
	//db_name
	$db_name = 'b57_php_ecommerce';

	$conn = mysqli_connect( $host, $db_username, $db_password, $db_name );
	// var_dump($conn); 
	//or 
	// ALTERNATIVE SYNTAX: 
	// define ('DB_HOST', 'localhost'); 
	// define('DB_USERNAME', 'root'); 
	// define ('DB_PASSWORD', '');
	// define ('DB_NAME', '#'): 

	// $conn = mysqli_connect(DB_HOST,DB_USERNAME,DB_PASSWORD_DB_NAME);

?>

