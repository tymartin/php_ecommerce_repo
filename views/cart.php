<?php
	
	require_once './../controllers/connection.php'; 
	require_once './../partials/template.php'; 
	// var_dump($_SESSION['cart']);  
	function get_content(){
	
	global $conn; ?> <!-- allows you to access $conn inside the function -->

	<div class="container my-4">
		<div class="row">
			<div class="col-lg-12">
				<h2>Cart Page</h2>
			</div>
		</div>
	<?php
	?> 
		<hr> 

		<div class="table-responsive">
			<table class="table table-striped table-bordered" id="cart-items">
				<thead>
					<tr>
						<th>Items</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>
						<th>User Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(isset($_SESSION['cart']) && count($_SESSION['cart']) > 0){
						$total = 0; 
						foreach($_SESSION['cart'] as $product_id => $product_quantity){
						// CREATE a query that will insert from products table the product that matches the product id.
						$sql_query = "SELECT * FROM products WHERE id = $product_id";
						// var_dump($sql_query); 
						$result = mysqli_query($conn, $sql_query); 
						// var_dump($result);  
						$indiv_product = mysqli_fetch_assoc($result); 
						// var_dump($indiv_product);
						
						//converts an associative array into a number of variable with names as the keys. 
						extract($indiv_product); 

						//we have now the following variables:
							//id
							//name
							//description
							//image 
							//price
							//category_id
						//creating formula for the subtotal 
						$subtotal = $price * $product_quantity; 
						$total += $subtotal; ?> 
				
					<tr>
						<td><?= $name ?></td>
						<td><?= $price ?> </td>
						<td><?= $product_quantity ?></td>
						<td><?= number_format($subtotal,2) ?></td>
						<td>
							<!-- edit quantity -->
							<form action="./../controllers/replace_cart_controller.php?id=<?= $id ?>" method="post">
								<label for="quantity">Edit Quantity</label>
								<input type="number" name="quantity" id="quantity" min="1" class="form-control form-control-sm" value="<?= $product_quantity ?>">
								<button class="btn btn-warning w-100 my-1">Change Qty.</button>
							</form>

							<!-- remove item from cart -->
							<a href="./../controllers/remove_from_cart_controller.php?id=<?= $id ?>" class="btn btn-danger w-100 my-1">Remove Item</a>
							
						</td>
					</tr>
				<?php } ?> 
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td> Total: <?= number_format($total, 2) ?></td>
						<td>
							<!-- payment modes -->
							<form action="./../controllers/create_transactions.php" method="POST">
								<div class="form-group">
									<label for="payment-mode">Choose Payment Method:</label>
									<select name="payment-mode" id="payment-mode" class="form-control">
										<?php
											$sql_query_get_payment_methods ="SELECT * FROM payment_modes"; 
											$result = mysqli_query($conn, $sql_query_get_payment_methods); 	
											while($mode = mysqli_fetch_assoc($result)){ ?>
												<option value="<?php echo $mode['id']; ?>">
													<?php echo $mode['name']; ?>
												</option>
											<?php }
										?>
									</select>	
									<button class="btn btn-success w-100 my-1">Checkout</button>
								</div>
							</form>
							
							<!-- clear cart -->
							<a href="./../controllers/clear_cart_controller.php" class="btn btn-outline-danger">Clear Cart</a>

						</td>
					</tr>
				<?php } else {
					echo "<tr>
							<td class='text-center' colspan='6'>No Products in Cart </td>
						</tr>"; 
				} ?>
				</tbody>
			</table>
		</div>
	</div>

<?php }; ?>