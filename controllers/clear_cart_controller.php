<?php

	//invoke the session cart
	session_start(); 

	//destroy the session
	unset($_SESSION['cart']); 

	//browser redirect to the previous page
	header("location: ./../views/cart.php"); 

?> 