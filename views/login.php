<?php
	
	require_once './../partials/template.php'; 
	
	function get_content(){?>
		<div class="container">
			<div class="row text-center my-3">
				<div class="col-md-4 offset-md-4">
					
				<!-- 	insert input form for users login page	 -->		
				<form action="./../controllers/authenticate.php" method="post">
					
					<div class="form-group">
						<label for="email">Enter Email Add:</label>
						<input type="email" name="email" id="email" class="form-control">
					</div>
					
					<div class="form-group">
						<label for="password">Enter Password:</label>
						<input type="password" name="password" id="password" class="form-control">
					</div>

					<button type="submit" class="btn btn-primary w-100">Login</button>
				</form>		
				</div>
			</div>		
		</div>
<?php }; ?> 

